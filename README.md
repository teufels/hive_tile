![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__tile-blue.svg)
![version](https://img.shields.io/badge/version-2.3.0-yellow.svg?style=flat-square)

HIVE > Tile
==========
Tile Content-Element which can contain Image, BG-Color, Title, Text, Link

#### This version supports TYPO3

![TYPO3Version](https://img.shields.io/badge/10_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_tile`

### Requirements
`none`

### How to use
- Install with composer
- Import Static Template (before hive_thm_custom)
- make own Layout by override Template in hive_thm_custom

### Notice
- developed with mask & mask_export

### Changelog
- 2.3.2 prepare for TYPO3 v12 LTS
- 2.3.1 add support for typo3 10
- 2.3.0 PHP8 fix
- 2.2.0 implement 'HIVE' CType Group
- 2.1.0 implement record_type for different Types
- 2.0.0 update on structure
- 1.1.0 inlcude JS/SCSS self (nogulp needed)
- 1.0.0 intial