<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_tile".
 *
 * Auto generated 25-09-2020 09:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['hive_tile'] = array (
    'title' => 'HIVE>Tile',
    'description' => 'HIVE Tile Content-Element',
    'category' => 'fe',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '2.3.3',
    'constraints' =>
    array (
        'depends' =>
        array (
          'typo3' => '10.4.0-0.0.0',
        ),
        'conflicts' =>
        array (
        ),
        'suggests' =>
        array (
        ),
    ),
);

