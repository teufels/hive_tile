<?php

namespace HIVE\HiveTile\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class LinkWizardViewHelper extends AbstractViewHelper
{


    public function initializeArguments()
    {
        $this->registerArgument('part', 'string', 'Parameter part to be extracted', true);
    }

    /**
     * @return string
     */
    public function render()
    {
        $part = $this->arguments['part'] ?? null;
        $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
        return $typoLinkCodec->decode($this->renderChildren())[$part] ?? '';
    }

}
