<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

call_user_func(function () {

//Adding Custom CType Item Group
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'CType',
    'hive',
    'HIVE',
    'after:special'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['hivetile_tile'] = 'tx_hivetile_tile';
$tempColumns = [
    'tx_hivetile_tileelement' => [
        'config' => [
            'appearance' => [
                'enabledControls' => [
                    'dragdrop' => '1',
                ],
                'levelLinksPosition' => 'both',
                'useSortable' => '1',
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_hivetile_tileelement',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
            'minitems' => 1,
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivetile_tileelement',
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    // Label
    'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tt_content.CType.hivetile_tile',
    // Value written to the database
    'hivetile_tile',
    // Icon
    'tx_hivetile_tile',
    // The group ID, if not given, falls back to "none" or the last used --div-- in the item array
    'hive'
];
$tempTypes = [
    'hivetile_tile' => [
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_hivetile_tileelement,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames, --palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
    ],
];
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

});

