<?php
return [
    'ctrl' => [
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'editlock' => 'editlock',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'translationSource' => 'l10n_source',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'title' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement',
        'label' => 'tx_hivetile_elementbackendtitle',
        'iconfile' => 'EXT:hive_tile/Resources/Public/Icons/Extension.svg',
        'hideTable' => true,
        'type' => 'record_type',
    ],
    'palettes' => [
        'language' => [
            'showitem' => '
                        sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l18n_parent
                    ',
        ],
        'hidden' => [
            'showitem' => '
                        hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden
                    ',
        ],
        'access' => [
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
            'showitem' => '
                        starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                        endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
                        --linebreak--,
                        fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
                        --linebreak--,editlock
                    ',
        ],
    ],
    'columns' => [
        'record_type' => [
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type.I.0',
                        '0',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tt_content.record_type.I.1',
                        'linked-element',
                    ],
                ],
            ],
        ],
        'editlock' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:editlock',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        '',
                        '',
                    ],
                ],
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tx_hivetile_tileelement',
                'foreign_table_where' => 'AND tx_hivetile_tileelement.pid=###CURRENT_PID### AND tx_hivetile_tileelement.sys_language_uid IN (-1, 0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => 2145913200,
                ],
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 20,
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                        -1,
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                        -2,
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                        '--div--',
                    ],
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
            ],
        ],
        'parentid' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1, ###REC_FIELD_sys_language_uid###)',
            ],
        ],
        'parenttable' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'sorting' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tx_hivetile_elementbackendtitle' => [
            'config' => [
                'eval' => 'required,trim',
                'placeholder' => 'Backendtitle',
                'type' => 'input',
            ],
            'description' => 'displayed in Backend only',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbackendtitle',
            'order' => 1,
        ],
        'tx_hivetile_elementbgcolor' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.0',
                        'transparent',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.1',
                        'primary',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.2',
                        'secondary',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.3',
                        'tertiary',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.4',
                        'quaternity',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.5',
                        'white',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.6',
                        'light',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.7',
                        'dark',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor.I.8',
                        'danger',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgcolor',
            'order' => 9,
        ],
        'tx_hivetile_elementbgimage' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_hivetile_elementbgimage',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,png,svg',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                        'parameters' => [
                            'allowedFileExtensions' => 'gif,jpg,jpeg,png,svg',
                        ],
                    ],
                ],
                'appearance' => [
                    'useSortable' => false,
                    'headerThumbnail' => [
                        'field' => 'uid_local',
                        'width' => '45',
                        'height' => '45c',
                    ],
                    'enabledControls' => [
                        'info' => true,
                        'new' => false,
                        'dragdrop' => true,
                        'sort' => false,
                        'hide' => true,
                        'delete' => true,
                    ],
                    'fileUploadAllowed' => false,
                ],
                'maxitems' => '1',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgimage',
            'order' => 7,
        ],
        'tx_hivetile_elementbgimageposition' => [
            'config' => [
                'default' => 'center center',
                'eval' => 'trim,lower',
                'type' => 'input',
            ],
            'description' => 'CSS background-position Property
e.g.: "center center", "x% y%", "xpos ypos"',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementbgimageposition',
            'order' => 8,
        ],
        'tx_hivetile_elementclass' => [
            'config' => [
                'eval' => 'trim,lower',
                'type' => 'input',
            ],
            'description' => 'Use for example bootstrap .order- classes for controlling the visual order of the element. 
(e.g.: order-1 order-lg-2) or use bootstrap display classes to show/hide element (e.g.: d-none d-lg-block)',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementclass',
            'order' => 11,
        ],
        'tx_hivetile_elementicon' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_hivetile_elementicon',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,png,svg',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                        'parameters' => [
                            'allowedFileExtensions' => 'gif,jpg,jpeg,png,svg',
                        ],
                    ],
                ],
                'appearance' => [
                    'useSortable' => false,
                    'headerThumbnail' => [
                        'field' => 'uid_local',
                        'width' => '45',
                        'height' => '45c',
                    ],
                    'enabledControls' => [
                        'info' => true,
                        'new' => false,
                        'dragdrop' => true,
                        'sort' => false,
                        'hide' => true,
                        'delete' => true,
                    ],
                    'fileUploadAllowed' => false,
                ],
                'maxitems' => '1',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementicon',
            'order' => 6,
        ],
        'tx_hivetile_elementitletype' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.default',
                        '',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.0',
                        'h1',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.1',
                        'h2',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.2',
                        'h3',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.3',
                        'h4',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.4',
                        'h5',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.5',
                        'h6',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.6',
                        'strong',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype.I.7',
                        'label',
                    ],
                ],
                'maxitems' => '1',
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitletype',
            'order' => 3,
        ],
        'tx_hivetile_elementitleposition' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitleposition.default',
                        '',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitleposition.I.0',
                        'text-left text-start',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitleposition.I.1',
                        'text-center',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitleposition.I.2',
                        'text-right text-end',
                    ],
                ],
                'maxitems' => '1',
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementitleposition',
            'order' => 3,
        ],
        'tx_hivetile_elementlink' => [
            'config' => [
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'title' => 'Link',
                        ],
                    ],
                ],
                'renderType' => 'inputLink',
                'softref' => 'typolink',
                'type' => 'input',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementlink',
            'order' => 5,
        ],
        'tx_hivetile_elementsize' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize.I.0',
                        'col-12',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize.I.1',
                        'col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize.I.2',
                        'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize.I.3',
                        'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4',
                    ],
                    [
                        'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize.I.4',
                        'col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementsize',
            'order' => 10,
        ],
        'tx_hivetile_elementtext' => [
            'config' => [
                'enableRichtext' => '1',
                'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
                'type' => 'text',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementtext',
            'order' => 4,
        ],
        'tx_hivetile_elementtitle' => [
            'config' => [
                'placeholder' => 'Title',
                'type' => 'input',
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_tile/Resources/Private/Language/locallang_db.xlf:tx_hivetile_tileelement.tx_hivetile_elementtitle',
            'order' => 2,
        ],
        't3_origuid' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
    ],
    'types' => [
        1 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,tx_hivetile_elementbackendtitle,tx_hivetile_elementtitle,tx_hivetile_elementitletype,tx_hivetile_elementitleposition,tx_hivetile_elementtext,tx_hivetile_elementlink,tx_hivetile_elementicon,tx_hivetile_elementbgimage,tx_hivetile_elementbgimageposition,tx_hivetile_elementbgcolor,tx_hivetile_elementsize,tx_hivetile_elementclass,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
        ],
        'linked-element' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,tx_hivetile_elementbackendtitle,tx_hivetile_elementlink,tx_hivetile_elementtitle,tx_hivetile_elementitletype,tx_hivetile_elementitleposition,tx_hivetile_elementtext,tx_hivetile_elementicon,tx_hivetile_elementbgimage,tx_hivetile_elementbgimageposition,tx_hivetile_elementbgcolor,tx_hivetile_elementsize,tx_hivetile_elementclass,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
            'columnsOverrides' => [
                'tx_hivetile_elementlink' => [
                    'config' => [
                        'eval' => 'required',
                    ],
                ],
            ],
        ],
    ],
];