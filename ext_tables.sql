CREATE TABLE tt_content (
    tx_hivetile_elementcontent_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_hivetile_elementcontent_parent (tx_hivetile_elementcontent_parent,pid,deleted),
    tx_hivetile_tileelement int(11) unsigned DEFAULT '0' NOT NULL
);
CREATE TABLE tx_hivetile_tileelement (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    record_type varchar(100) NOT NULL DEFAULT '0',
    tx_hivetile_elementbackendtitle tinytext,
    tx_hivetile_elementbgcolor tinytext,
    tx_hivetile_elementbgimage int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivetile_elementbgimageposition tinytext,
    tx_hivetile_elementclass tinytext,
    tx_hivetile_elementicon int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivetile_elementitletype tinytext,
    tx_hivetile_elementitleposition tinytext,
    tx_hivetile_elementlink tinytext,
    tx_hivetile_elementsize tinytext,
    tx_hivetile_elementtext mediumtext,
    tx_hivetile_elementtitle tinytext,
    KEY language (l10n_parent,sys_language_uid)
);
